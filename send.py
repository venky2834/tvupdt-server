'''
This script sends a JSON object to a registered user whose ID is received by a POST request.
Also an IMDb tvid is received

1. tvid and regid is extracted from POST request.
2. Corresponding entry from Database Series is extracted.
3. JSON object is formed.
4. Message is sent via GCM
'''

import webapp2
import json
import logging
import urllib2
from google.appengine.ext import db

class Series(db.Model):
    tvid = db.StringProperty(required=True)
    title = db.StringProperty(required=True)
    status = db.IntegerProperty()
    epname = db.StringProperty()
    epinfo = db.StringProperty()
    epdate = db.StringProperty()
    rely = db.IntegerProperty()
    up_cycle = db.IntegerProperty()

class Send(webapp2.RequestHandler):
    def post(self):
        logging.info("POST request received")
        one = False
        two = False
        
        # Step 1 : Extracting tvid and regid from POST Request
        
        reqBody = self.request.body
        body = json.loads(reqBody)

        if(body.has_key('regid') and body.has_key('tvid')):
            rregid = body['regid']
            rtvid = body['tvid']
            logging.debug("received regid = " + rregid)
            logging.debug("received tvid = " + rtvid)
            one = True

        elif(body.has_key('regid')):
            logging.error("Improper POST request. Key 'tvid' not present.")
            self.response.write("Improper POST request. Key 'tvid' not present.")

        elif(body.has_key('tvid')):
            logging.error("Improper POST request. Key 'regid' not present.")
            self.response.write("Improper POST request. Key 'regid' not present.")

        else:
            logging.error("Improper POST request. Key 'tvid' and 'regid' not present.")
            self.response.write("Improper POST request. Key 'tvid' and 'regid' not present.")

        #Step 1 Complete

        #Step 2 : Extract corresponding entry from Series database
        # This step is only undertaken when previous step has succeeded

        if(one == True):
            q = Series.all()
            q.filter('tvid =',rtvid)
            entry = q.get()

            if(entry == None):
                logging.error("Entry corresponding to IMDb id : " + str(rtvid) + " is not present in DB")
                self.response.write('Invalid tvid')
            else:
                s_tvid = entry.tvid
                s_title = entry.title
                s_status = entry.status
                s_rely = entry.rely
                s_epname = entry.epname
                s_epinfo = entry.epinfo
                s_epdate = entry.epdate

                logging.debug("Entry extracted from Database")
                logging.info("title : " + s_title)
                logging.info("tvid : " + s_tvid)
                logging.info("status : " + str(s_status))
                logging.info("rely : " + str(s_rely))
                logging.info("epname : " + s_epname)
                logging.info("epinfo : " + s_epinfo)
                logging.info("epdate : " + s_epdate)

                two = True

        #Step 2 Complete
                
        #Step 3 : Make a JSON object from the data obtained
        # This step is undertaken when step 2 has succeeded

        if(two == True):
            series_info = {
                "tvid" : s_tvid,
                "title" : s_title,
                "status" : s_status,
                "rely" : s_rely,
                "epname" : s_epname,
                "epinfo" : s_epinfo,
                "epdate" : s_epdate
                }
            payload = {'data' : series_info,
                        'registration_ids' : [rregid]
                       }
            json_data = json.dumps(payload)

        #Step 3 Complete

        #Step 4: Send the message using GCM
        # This step is undertaken when step 2 has succeded
        if(two == True):
            url = "https://android.googleapis.com/gcm/send"

            headers = {'Content-Type': 'application/json',
                       'application' : 'x-www-form-urlencoded',
                       'charset' : 'UTF-8',
                       'Authorization':'key=AIzaSyC6C6D1lwgkxVd90Dph_sZV3qxQN2tykeM'
                       }

            postreq = urllib2.Request(url, json_data, headers)

            r = urllib2.urlopen(postreq)
            response = r.read()

            res = json.loads(response)

            logging.debug('Multicast ID of message : ' + str(res['multicast_id']))
            
            if(res['failure'] == 1):
                logging.error("Error while Sending Message via GCM")
                error = res['results'][0]['error']
                logging.error('Error : ' + error) # Log the error code
                self.response.write(error)
            else:
                m_id = res['results'][0]['message_id']
                logging.info("Message Sent")
                logging.info("message id : " + str(m_id)) # Log the message ID
                self.response.write("Sent")

        # Step 4 Complete

    def get(self):
        logging.info("GET Request received")
        self.response.write("Link to send GCM")
        
application = webapp2.WSGIApplication([('/send', Send),],debug=True)
