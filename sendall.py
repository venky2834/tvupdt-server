'''
This script on receiving a registration ID, undertakes the following steps.

1. Extracts the preference entry from UserPrefs database corresponding to regid.
2. Make a list of imdb id of those series which the user requires.(i.e. preference 1)
3. Make a JSON object of regid and imdb id for each entry in list and send them to
   user through GCM one by one

'''
import time
import webapp2
import logging
import urllib2
import json
from google.appengine.ext import db

class UserPrefs(db.Model):
        regid = db.StringProperty(required=True)
        grey = db.IntegerProperty() #Grey's Anatomy
        bones = db.IntegerProperty() #Bones
        himym = db.IntegerProperty() #How I Met Your Mother
        dexter = db.IntegerProperty() #Dexter
        burn = db.IntegerProperty() #Burn Notice
        bbt = db.IntegerProperty() # The Big Band Theory
        bbad = db.IntegerProperty() # The Breaking Bad
        naruto = db.IntegerProperty() # Naruto Shippuden
        mentl = db.IntegerProperty() # The Mentalist
        castle = db.IntegerProperty() # Castle
        glee = db.IntegerProperty() # Glee
        vamp = db.IntegerProperty() # The Vampire's Diary
        walk = db.IntegerProperty() # The Walking Dead
        suits = db.IntegerProperty() # Suits
        home = db.IntegerProperty() # Homeland

class SendAll(webapp2.RequestHandler):
    def post(self):
        logging.info('POST request received')
        # a dictionary with short names of series as key and IMDb id as value
        
        tv_dic = {
                'castle' : '1219024',
                'bbad' : '0903747',
                'dexter': '0773262',
                'suits': '1632701',
                'bbt': '0898266',
                'himym': '0460649',
                'burn': '0810788',
                'naruto': '0988824',
                'bones': '0460627',
                'walk': '1520211',
                'vamp': '1405406',
                'mentl': '1196946',
                'glee': '1327801',
                'home': '1796960',
                'grey': '0413573'
                }
        one = False
        
        # Step 1: Extracting preferences and making a list of imdb movie id of preference
        reqBody = self.request.body
        body = json.loads(reqBody)

        if(not body.has_key('regid')):
            logging.error('Improper POST request. Key "regid" not present')
            self.response.write('Improper request')
        else:
            r_regid = body['regid']
            logging.info("regid : " + r_regid)

            q = UserPrefs.all()
            q.filter('regid =',r_regid)
            pref = q.get()

            if(pref == None):
                logging.error("User is not Registered")
                self.response.write('Unregistered')
            else:
                usertv = []
                if(pref.home == 1):
                        usertv.append(tv_dic['home'])
                if(pref.suits == 1):
                        usertv.append(tv_dic['suits'])
                if(pref.walk == 1):
                        usertv.append(tv_dic['walk'])
                if(pref.vamp == 1):
                        usertv.append(tv_dic['vamp'])
                if(pref.glee == 1):
                        usertv.append(tv_dic['glee'])
                if(pref.castle == 1):
                        usertv.append(tv_dic['castle'])
                if(pref.mentl == 1):
                        usertv.append(tv_dic['mentl'])
                if(pref.naruto == 1):
                        usertv.append(tv_dic['naruto'])
                if(pref.bbad == 1):
                        usertv.append(tv_dic['bbad'])
                if(pref.bbt == 1):
                        usertv.append(tv_dic['bbt'])
                if(pref.burn == 1):
                        usertv.append(tv_dic['burn'])
                if(pref.dexter == 1):
                        usertv.append(tv_dic['dexter'])
                if(pref.himym == 1):
                        usertv.append(tv_dic['himym'])
                if(pref.bones == 1):
                        usertv.append(tv_dic['bones'])
                if(pref.grey == 1):
                        usertv.append(tv_dic['grey'])

                one = True
                logging.info('List of imdb movie id selected by user : ' + str(usertv))
                logging.info('User preferences extracted')

        # Step 1 Complete        

        # Step 2. Make a json object for each of the item and send it to user.
        if(one == True):
            url = 'http://tvupdt.appspot.com/send'
            headers = {'Content-Type': 'application/json'}
            logging.debug('Starting to send info to user')
            
            for tv in usertv:
                logging.info('imdb movie id : ' + tv)    
                payload = {'regid' : r_regid,
                           'tvid' : tv}
                data = json.dumps(payload)
                req = urllib2.Request(url, data, headers)
                f = urllib2.urlopen(req)
                response = f.read()
                f.close()
                
                if(response == 'Sent'):
                    logging.debug('Message sent')
                else:
                    logging.error('Message sending failed')
                    logging.error(response)
                    #usertv.append(tv) # Try again in a cycle
                

        self.response.write('Operation Performed')
        
    def get(self):
        logging.info('GET request received')
        self.response.write('Link to Set Preferences')
        
application = webapp2.WSGIApplication([('/sendall', SendAll),],debug=True)
